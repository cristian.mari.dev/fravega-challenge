# Aclaraciones
Lamento unicamente el .forceUpdate() pero el resultado es el esperado.

# Fravega Challenge

Challenge que lleva como proposito desarrollar un listado de pokemon's.

## Comenzando

Estas instrucciones le proporcionarán una copia del proyecto en funcionamiento en su máquina local para fines de desarrollo y prueba.

### Pre-requisitos

Qué cosas necesita para instalar la aplicación y cómo instalarlas

* [Instalar Git](/git-readme.md)

* [Instalar Node](https://nodejs.org/es/download/)


### Instalación

Primero realizar la clonación del proyecto en una carpeta destinada al desarrollo de las aplicaciones.

Ubiquece dentro de la consola en su carpeta destino y ejecute el siguiente codigo :
```
git clone https://gitlab.com/cristian.mari.dev/fravega-challenge.git
```


### Instalar dependencias

Abra una terminal y ubiquese dentro de la carpeta que se clono del proyecto, ejecute el siguiente comando:
```
npm install
```


## Comandos de la aplicación

| Comando  |  Descripción |  
|---|---|
| dev  |  Inicializa la aplicación en modo desarrollo tendrá actualización en vivo y funcionalidades que le ayudaran con sus tareas.  |  
|  build |  Inicializa la construcción del empaquetado de su sitio web optimizado para ambitos productivos. | 
|  build:gitlab |  Inicializa la construcción del empaquetado de su sitio web optimizado para deploy en gitlab pages. | 
| test | Inicializa el set de pruebas de las pantallas y componententes.

### Ejemplo del uso de los comandos:

```
npm run dev (o) yarn dev << Si tiene usa yarn >>
npm run build (o) yarn build  << Si tiene usa yarn >>
```
