/**
 * @desc Idiomas
 */
export const LanguagesAvailables = { 
    "ES": {
        "code": "es",
        "label": "Español"
    },
    "EN": {
        "code": "en",
        "label": "Ingles"
    }
};
