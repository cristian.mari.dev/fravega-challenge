/**
 * @desc Dependencias
 */
import { createGlobalStyle } from 'styled-components'

/**
 * @desc Estilos globales
 */
export default createGlobalStyle`
    *{
        font-family: 'Roboto' ;
        outline:0px;
    }
    html{
        height:100%;
    }
    body{
        background:#eee;
        height:100%;
        margin:0px;
    }
    #root{
        height:100%;
    }
`
