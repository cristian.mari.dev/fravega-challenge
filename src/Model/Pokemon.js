/**
 * @desc Modelo
 */
class PokemonModel {

    /**
     * @desc Obtenemos el listado de los pokemon's
     * 
     * @return { Promise<Object> }
     */
    static async getAll( current = 1, limit = 5 ){

        // Solicitud 
        const request = await fetch( `https://pokeapi.co/api/v2/pokemon?limit=${ limit }&offset=${ current * limit }` );

        return await request.json();

    }


    /**
     * @desc Obtenemos el listado de los pokemon's
     * 
     * @return { Promise<Object> }
     */
    static async get( name = '' ){

        // Solicitud 
        const request = await fetch( `https://pokeapi.co/api/v2/pokemon/${ name }` );

        return await request.json();

    }



    /**
     * @desc Obtenemos el detalle de una abilidad pokemon's
     * 
     * @return { Promise<Object> }
     */
    static async getAbilityOfPokemon( id = '' ){

        // Solicitud 
        const request = await fetch( `https://pokeapi.co/api/v2/ability/${ id }` );

        return await request.json();

    }

    /**
     * @desc Obtenemos el detalle de un atributo del pokemon's
     * 
     * @return { Promise<Object> }
     */
    static async getStatOfPokemon( id = '' ){

        // Solicitud 
        const request = await fetch( `https://pokeapi.co/api/v2/stat/${ id }` );

        return await request.json();

    }
    
}

export default PokemonModel;