'use strict';

/**
 * @desc Dependencias
 */
const fetch = require('node-fetch');
const Bluebird = require('bluebird');

/**
 * @desc Verificamos la exitencia de la interfaz.
 */
if ( !global.fetch ) {
    fetch.Promise = Bluebird;
    global.fetch = fetch;
}
