/**
 * @desc Dependencias
 */
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

/**
 * @desc Rutas.
 */
import App from "./Containers/Home/Home";

/**
 * @desc Diseño
 */
import GlobalStyles from "./root.styles";

/**
 * @desc Renderiza la aplicación.
 */
ReactDOM.render(
    <BrowserRouter>
        <GlobalStyles />
        <App />
    </BrowserRouter>, 
document.getElementById('root'));