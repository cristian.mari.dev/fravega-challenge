/**
 * @desc Dependencias 
 */
import { Component } from "react";
import PokemonModel from "../../Model/Pokemon";

/**
 * @desc Traductor
 */
import Translate from "../../Languages";

/**
 * @desc Utilidades
 */
import { LanguagesAvailables } from "../../Utils/Constants";

/**
 * @desc Controlador de la pantalla del listado de pokemon's
 */
class HomeController extends Component{

    /**
     * @desc Constructor
     * 
     * @param { Object } props
     * 
     * @return { void }
     */
    constructor( props ){

        super( props );

        // Estado actual.
        this.state = {

            // Listado de pokemon's
            pokedex: {
                data: null,
                selected: null,
                current: 0,
                pages: 0
            },

            // Indica si se esta cargando la pantalla
            loading: true,

            // Lenguaje utilizado 
            languageSelected: LanguagesAvailables.ES

        };

    }

    /**
     * @desc Ciclo de vida: Componente montado.
     */
    async componentDidMount(){

        // Obtenemos todos los pokemon.
        await this.getAllPokemons();

    }

    /**
     * @desc Actualiza el estado de forma sincrona.
     * 
     * @return { Promise }
     */
    setStateSync( newState ){

        return new Promise( ( resolve, reject ) => {

            try{

                // Actualiza el estado
                this.setState( newState, resolve );

                resolve( true );

            }catch( error ){

                // Logueamos el error
                process.env.DEBUG &&
                    console.log( error );

                reject( false );

            }

        })

    }

    /**
     * @desc Obtenemos el listado de los pokemon
     * 
     * @return { Promise<Boolean> }
     */
    async getAllPokemons(){

        try{

            // Alias del estado
            const { pokedex } = this.state;

            // Indicamos que se esta cargando.
            let loading = true;

            // Actualizamos el indicador de carga
            await this.setStateSync({ loading });

            // Resultado
            const response = await PokemonModel.getAll( pokedex.current, 5 );

            // Verificamos el resultado de la respuesta
            if( response.results ){

                // Alias de la respuesta
                const { results: data } = response;

                // Pokedex
                let pokedex = {
                    ...this.state.pokedex,
                    "data": await this.generateScheme( data )
                };

                // Indicamos que se finalizo la carga
                loading = false;

                // Actualizamos el estado
                await this.setStateSync({ loading, pokedex });

            }

            return true;

        }catch( error ){

            // Logueamos el error
            process.env.DEBUG &&
                console.log( error );

            return false;

        }

    }

    /**
     * @desc Generamos el esquema para la pantalla.
     * 
     * @param { Array } data
     * 
     * @return { Array }
     */
    async generateScheme( data = [] ){

        try{

            // Verificamos los datos requeridos
            if( !Array.isArray( data ) )
                throw new TypeError(".generateScheme() expected a param of type Array call data, but received a type " + typeof data );

            // Resultado
            let result = [];

            // Recorremos los datos
            for( let pokemonShort of data ){

                // Verificamos que exista el nombre
                if( typeof pokemonShort === "object" && pokemonShort.hasOwnProperty("name") ){

                    // Pokemon
                    const responseData = await PokemonModel.get( pokemonShort.name );
                    
                    // Datos del pokemon
                    result.push( responseData );

                }

            }

            return result;

        }catch( error ){

            // Logueamos el error
            process.env.DEBUG &&
                console.log( error );

            return false;

        }

    }

    /**
     * @desc Indica un pokemon que debe visualizar 
     * 
     * @param { Object } pokemonSelected
     * 
     * @return { Boolean }
     */
    async openDetailPokemon( pokemonSelected ){

        try{

            // Verifica los parametros requeridos.
            if( pokemonSelected === null || typeof pokemonSelected !== "object" || Array.isArray( pokemonSelected ) )
                throw new TypeError( ".openDetailPokemon() expected a param of type Object call pokemonSelected, but received a " + typeof pokemonSelected );

            // Alias del estado
            let { pokedex } = this.state;

            // Actualizamos el estado
            pokedex.selected = await this.getDetailsOfPokemon( pokemonSelected );

            // Actualizamos el estado
            this.setState({ pokedex });

            return true;

        }catch( error ){

            // Logueamos el error
            process.env.DEBUG &&
                console.log( error );

            return false;

        }

    }
    
    /**
     * @desc Indica un pokemon que debe visualizar 
     * 
     * @return { Boolean }
     */
    closePokemonDetail( ){

        try{

            // Alias del estado
            let { pokedex } = this.state;

            // Actualizamos el estado
            pokedex.selected = null;

            // Actualizamos el estado
            this.setState({ pokedex });

            return true;

        }catch( error ){

            // Logueamos el error
            process.env.DEBUG &&
                console.log( error );

            return false;

        }

    }
    

    /**
     * @desc Obtenemos el detalle de un pokemon
     * 
     * @return { Promise<Boolean> }
     */
    async getDetailsOfPokemon( pokemon ){

        try{

            // Verifica los parametros requeridos.
            if( pokemon === null || typeof pokemon !== "object" || Array.isArray( pokemon ) )
                throw new TypeError( ".getDetailsOfPokemon() expected a param of type Object call pokemon, but received a " + typeof pokemonSelected );

            // Pokemon clone
            let clone = {
                ...pokemon
            }

            // Buscamos las abilidades del pokemon
            clone.abilities = await this.getAbilitiesOfPokemon( pokemon );
            clone.stats     = await this.getStatOfPokemon( pokemon );

            return clone;

        }catch( error ){

            // Logueamos el error
            process.env.DEBUG &&
                console.log( error );

            return false;

        }

    }

    /**
     * @desc Obtenemos el listado de los pokemon
     * 
     * @return { Promise<Boolean> }
     */
    async getAbilitiesOfPokemon( pokemon ){

        try{

            // Resultado
            let result = [];

            // Obtenemos las habilidades
            for ( let property of pokemon.abilities ){

                // Respuesta
                let response = await PokemonModel.getAbilityOfPokemon( property.name ? property.name : property.ability.name );

                // Añadimos el item
                result.push({
                    "name": response.name,
                    "description": {
                        "es": this.extractLanguage( "es", response.flavor_text_entries ),
                        "en": this.extractLanguage( "en", response.flavor_text_entries ),
                    },
                    "language":{
                        "es": this.extractLanguage( "es", response.names ),
                        "en": this.extractLanguage( "en", response.names )
                    }
                });

            } 

            return result;

        }catch( error ){

            // Logueamos el error
            process.env.DEBUG &&
                console.log( error );

            return false;

        }

    }

    /**
     * @desc Obtenemos el listado de los pokemon
     * 
     * @return { Promise<Boolean> }
     */
    async getStatOfPokemon( pokemon ){

        try{

            // Resultado
            let result = [];

            // Obtenemos las habilidades
            for ( let stadistic of pokemon.stats ){

                // Respuesta
                let response = await PokemonModel.getStatOfPokemon( stadistic.name ? stadistic.name : stadistic.stat.name );

                // Añadimos el item
                result.push({
                    "name": response.name,
                    "value": stadistic.base_stat,
                    "language":{
                        "es": this.extractLanguage( "es", response.names ),
                        "en": this.extractLanguage( "en", response.names )
                    }
                });

            } 

            return result;

        }catch( error ){

            // Logueamos el error
            process.env.DEBUG &&
                console.log( error );

            return false;

        }

    }

    /**
     * @desc Extrae las traducciónes del recurso
     * 
     * @param { String } language_code
     * @param { Array } resources
     * 
     * @return { Object }
     */
    extractLanguage( language_code, resources ){

        try{

            // Verificamos los parametros requeridos
            if( typeof language_code !== "string" )
                throw new Error( ".extractLanguage() expected a param of type string call language_code, but received " + typeof language_code.toUpperCase() )

            // Verificamos los parametros requeridos
            if( !LanguagesAvailables.hasOwnProperty( language_code.toUpperCase() ) )
                throw new Error( ".extractLanguage() expected a language_code valid, but received " + language_code.toUpperCase() )

            // Verificamos los parametros requeridos
            if( !Array.isArray(resources) )
                throw new Error( ".extractLanguage() expected a param resources of type Array, but received " + typeof resources )

            // Recorremos los recursos
            for( let resource of resources ){
                if( resource.hasOwnProperty("language") && resource.language.hasOwnProperty("name") && resource.language.name.toUpperCase() === language_code.toUpperCase() ){
                    return resource;
                }
            }

            return null;

        }catch( error ){

            // Logueamos el error
            process.env.DEBUG &&
                console.log( error );

            return null; 

        }

    }

    /**
     * @desc Navegación Previa
     * 
     * @return { Boolean }
     */
    async prevPagination(){

        try{

            // Alias del estado
            let { pokedex } = this.state;

            // Indicamos la nueva paginación
            pokedex.current = pokedex.current > 0 ? pokedex.current - 1 : 0;

            // Actualizamos el estado
            await this.setStateSync({ pokedex });

            // Obtenemos la lista de pokemon's
            await this.getAllPokemons();

            return true;

        }catch( error ){

            // Logueamos el error
            process.env.DEBUG &&
                console.log( error );

            return false;

        }

    }
    /**
     * @desc Proxima navegación
     * 
     * @return { Boolean }
     */
    async nextPagination(){

        try{

            // Alias del estado
            let { pokedex } = this.state;

            // Indicamos la nueva paginación
            pokedex.current = pokedex.current + 1;

            // Actualizamos el estado
            await this.setStateSync({ pokedex });

            // Obtenemos los pokemon's
            await this.getAllPokemons();

            return true;

        }catch( error ){

            // Logueamos el error
            process.env.DEBUG &&
                console.log( error );

            return false;

        }

    }

    /**
     * @desc Cambia el lenguaje seleccionado
     * 
     * @param { String } languageSelected
     * 
     * @return { Boolean }
     */
    changeLanguage( languageSelected ){

        try{

            // Actualizamos el estado
            this.setState({ languageSelected }, () => {
                // Indicamos el nuevo lenguaje.
                Translate.loadLanguage( languageSelected.code );
                // TODO: Usar una libreria mejor.
                this.forceUpdate();
            });

            return true;

        }catch( error ){

            // Logueamos el error
            process.env.DEBUG &&
                console.log( error );

            return false;
        }

    }

}

export default HomeController;