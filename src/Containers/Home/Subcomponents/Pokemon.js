/**
 * @desc Dependencias
 */
import React from "react";

/**
 * @desc Traducciones
 */
import Translate from "../../../Languages";

/**
 * @desc Estilos.
 */
import {
     Root, 
     Side, 
        Picture, 
     
     Info,
         Name, Propertys,
            Row, Label, Value
    } from "./Pokemon.styles";

/**
 * @desc Componente del pokemon
 */
export default ({ ...props }) => {

    // Alias de las propiedades
    const { 
        // Datos del pokemon.
        data = {},
        // Evento de apertura de detalle.
        onSelect = () => {}
    } = props;

    return (
        <Root onClick={ () => onSelect( data ) }>

            <Side>

                { /* Imagen del pokemon */ }
                <Picture src={ "https://img.pokemondb.net/sprites/black-white/anim/normal/"+data.name+".gif" } />

            </Side>

            { /* Información */ }
            <Info>

                { /* Nombre del pokemon */ }
                <Name onClick={ () => onSelect( data ) }>{ data.name }</Name>

                { /* Propiedades */ }
                <Propertys>

                    <Row>
                        <Label>{ Translate.parse("HEIGHT") }:</Label>
                        <Value>{ parseFloat(data.height / 10).toFixed(2) } mts</Value>
                    </Row>
                    <Row>
                        <Label>{ Translate.parse("WEIGHT") }:</Label>
                        <Value>{ parseFloat(data.weight / 10).toFixed(2) } k</Value>
                    </Row>

                </Propertys>

            </Info>

        </Root>
    );

};