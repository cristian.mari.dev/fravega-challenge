/**
 * @desc Dependencias
 */
import styled, { css } from "styled-components";

/**
 * @desc Raiz
 */
export const Root = styled.div`
    display:flex;
    width:450px;
    max-width:88%;
    margin:10px auto;
    padding:5px 20px;
    border-radius:4px;
    flex-direction:row;
    align-items:center;
    @media (max-width: 480px) {
        flex-direction: row;
        flex-wrap:wrap;
    }
    cursor:pointer;
`;

/**
 * @desc Nombre del pokemon
 */
export const Name = styled.h1`
    text-transform: capitalize;
    margin:0px;
    padding:10px;
    cursor:pointer;
`;

/**
 * @desc Imagen del pokemon
 */
export const Picture = styled.figure`
    ${
        props => props.src &&
            css `
                background-image:url(${ props.src });
                background-size:40%;
                background-repeat:no-repeat;
                background-position:center;
            `
    }
    width:100%;
    height:100%;
    position:absolute;
    top:0;
    left:0;
    margin:0;
`;

/**
 * @desc Lateral
 */
export const Side = styled.aside`
    display:flex;
    background:white;
    width:180px;
    height:100px;
    box-shadow:0px 0px 10px -3px rgba(0,0,0,0.25);
    text-align:center;
    margin:5px 20px;
    padding:0;
    border-bottom:2px solid #333;
    border-radius:3px;
    justify-content:center;
    align-items:center;
    position:relative;
    @media (max-width: 480px) {
        background:#fff;
        width:80px;
        height:80px;
        border-radius:50%;
        margin:0px auto;
        z-index:2;
        position:relative;
        bottom:-40px;
        border:2px solid #333;
    }
`;

/**
 * @desc Información del pokemon
 */
export const Info = styled.div`
    background:#e4e4e4;
    width:100%;
    @media (max-width: 480px) {
        padding-top:30px;
        border-top:2px solid #333;
    }
`;



/**
 * @desc Tabla de propiedades
 */
export const Propertys = styled.div`
`;


/**
 * @desc Fila de la tabla
 */
export const Row = styled.div`
    display:flex;
    flex-wrap:nowrap;
    border-bottom:1px solid #ccc;
    align-items:center;
`;

/**
 * @desc Etiqueta de la propiedad
 */
export const Label = styled.h1`
    width:50%;
    margin:0;
    padding:10px;
    font-size:1rem;
`;

/**
 * @desc Etiqueta de la propiedad
 */
export const Value = styled.p`
    width:50%;
    margin:0;
    padding:10px;
    font-size:1rem;
`;
