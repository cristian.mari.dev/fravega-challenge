/**
 * @desc Dependencias
 */
import styled, { css } from "styled-components";

/**
 * @desc Asset's
 */
import BannerImage from "../../../Assets/img/banner.png";

/**
 * @desc Raiz
 */
export const Root = styled.div`
    display:flex;
    width:100%;
    height:100%;
    padding:5px 0px;
    border-radius:4px;
    flex-direction:row;
    align-items:center;
    justify-content:center;
    position:fixed;
    top:0;
    left:0;
    z-index:9;
    @media (max-width: 420px){
        padding:5px 0px;
        top:60px;
    }
`;

/**
 * @desc Contenido
 */
export const Content = styled.div`
    background:#fff;
    width:90%;
    max-width:700px;
    z-index:2;
    padding-top:80px;
    margin-bottom:20px;
    border-radius:4px;
    @media (max-width: 420px){
        padding-top:40px;
    }
`;

/**
 * @desc Raiz
 */
export const Overlay = styled.div`
    display:flex;
    background:rgba(0,0,0,0.5);
    width:100%;
    height:100%;
    position:fixed;
    top:0;
    left:0;
`;

/**
 * @desc Nombre del pokemon
 */
export const Name = styled.h1`
    text-transform: capitalize;
    margin:0px;
    padding:10px;
`;

/**
 * @desc Imagen del pokemon
 */
export const Picture = styled.figure`
    background: white;
    padding: 20px;
    width: 50px;
    height: 50px;
    border: 4px solid #333;
    border-radius: 50%;
    position: absolute;
    top: -64px;
    left:calc(50% - 45px);
    margin:0px;
    ${
        props => props.src &&
            css `
                background-image:url(${ props.src });
                background-size:50%;
                background-repeat:no-repeat;
                background-position:center;
            `
    }
`;

/**
 * @desc Lateral
 */
export const Header = styled.aside`
    display:flex;
    background:white;
    background-image:url(${ BannerImage });
    height:100px;
    text-align:center;
    margin:0px auto;
    margin-top:-120px;
    padding:0;
    justify-content:center;
    align-items:center;
    box-shadow:0px 0px 10px -2px rgba(0,0,0,0.5);
    border-top:4px solid #333;
    position:relative;
    @media (max-width:420px){
        height:80px;
    }
`;

/**
 * @desc Información del pokemon
 */
export const Info = styled.div`
    background:#fff;
    width:100%;
    max-height:60vh;
    overflow-y:auto;
    border-radius:4px;
`;



/**
 * @desc Tabla de propiedades
 */
export const Abilities = styled.div`
    display:flex;
    background:#444;
    justify-content:flex-start;
    flex-wrap:nowrap;
    & div{
            color:white;
    }
    @media ( max-width: 480px ){
        flex-wrap:wrap;
    }

`;


/**
 * @desc Habilidad
 */
export const Ability = styled.div`
    width:100%;
`;


/**
 * @desc Estadisticas
 */
export const Stats = styled.div`
    display:flex;
    justify-content:flex-start;
    flex-wrap:wrap;
    & div{
        flex-basis:33.3333%;
    }
    @media (max-width: 420px){
        & div{
            flex-basis:50%;
        }
    }
`;


/**
 * @desc Fila de la tabla
 */
export const Row = styled.div`
    display:flex;
    width:100%;
    flex-wrap:nowrap;
    border-bottom:1px solid #ccc;
    align-items:center;
`;

/**
 * @desc Etiqueta de la propiedad
 */
export const Label = styled.h1`
    width:50%;
    margin:0;
    padding:10px;
    font-size:1rem;
`;

/**
 * @desc Etiqueta de la propiedad
 */
export const Value = styled.p`
    width:50%;
    margin:0;
    padding:10px;
    font-size:1rem;
    align-items:center;
    vertical-align:center;
`;


/**
 * @desc Etiqueta de la propiedad
 */
export const AbilityName = styled.p`
    margin:0px;
    padding:10px;
    padding-bottom:0px;
`;

/**
 * @desc Etiqueta de la propiedad
 */
export const Description = styled.p`
    color:#ccc;
    font-family:monospace;
    margin:0;
    padding:10px;
    padding-top:5px;
`;

/**
 * @desc Etiqueta de la propiedad
 */
export const Nav = styled.p`
    display:flex;
    justify-content:center;
    align-items:center;
`;


/**
 * @desc Boton de cerrar
 */
export const Button = styled.button`
    background:#06c;
    color:white;
    padding:10px 20px;
    border:0px;
    margin:0px auto;
    margin-bottom:-40px;
    cursor:pointer;
    border-radius:3px;
    &:hover{
        background:#05b;
    }
    &:active{
        background:#04a;
    }
`;
