/**
 * @desc Dependencias
 */
import React from "react";
import { shallow } from "enzyme";
import 'jest-styled-components'

/**
 * @desc Contenedor
 */
import PokemonDetail from "./PokemonDetail";

/**
 * @desc Suite de test's
 */
describe("[PokemonDetail]", () => {
    
	/**
	 * @desc Testeamos el snapshot.
	 */
    it("[Component.PokemonDetail] Verificamos el renderizado del componente Pokemon", () => {

        // Renderizamos el contenedor.
        const rendered = shallow( <PokemonDetail /> );

        // Comparamos el snapshot
        expect( rendered ).toMatchSnapshot();

    });


});
