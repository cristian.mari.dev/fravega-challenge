/**
 * @desc Dependencias
 */
import React, { Fragment } from "react";

/**
 * @desc Traductor.
 */
import Translate from "../../../Languages";

/**
 * @desc Estilos.
 */
import {
     Root, 
     Overlay,
     Header, 
        Picture, 
     Content,
     Info,
         Name, Abilities, Ability, AbilityName,
         Stats,
            Row, Label, Value,
            Description,
         Nav, Button
    } from "./PokemonDetail.styles";

/**
 * @desc Componente del pokemon
 */
export default ({ ...props }) => {

    // Alias de las propiedades
    const { 
        // Datos del pokemon
        data = {}, 
        // Evento de cierre
        onClose = () => {},
        // Lenguaje
        language 
    } = props;

    return (
        <Root>

            <Overlay onClick={ () => onClose() } />

            <Content>

                { /* Cabecera */ }
                <Header>

                    { /* Imagen del pokemon */ }
                    <Picture src={ "https://img.pokemondb.net/sprites/black-white/anim/normal/"+data.name+".gif" } />

                </Header>

                { /* Información */ }
                <Info>

                    { /* Nombre del pokemon */ }
                    <Name>{ data.name }</Name>

                    { /* Propiedades */ }
                    <Label>{ Translate.parse("ABILITIES") }:</Label>
                    <Abilities>

                        {   /* Recorremos las abilidades */
                            Array.isArray( data.abilities ) && data.abilities.map( ( value, key ) => (
                                <Ability key={ key }>
                                    <AbilityName>
                                        { /* Nombre de la habilidad */ }
                                        <b>{ value.language[language].name }</b>
                                    </AbilityName>
                                    { /* Información de la habilidad */ }
                                    <Description>{ value.description[language].flavor_text }</Description>
                                </Ability>
                            ))
                        }

                    </Abilities>


                    { /* Propiedades */ }
                    <Stats>

                        {
                            Array.isArray( data.stats ) && data.stats.map( ( stat, key ) => (
                                    <Row key={ key }>
                                        <Label>{ stat.language[language].name }</Label>
                                        <Value>{ stat.value }</Value>
                                    </Row>
                            ))

                        }

                    </Stats>

                </Info>

                <Nav>
                    <Button onClick={ () => onClose() }>{ Translate.parse("CLOSE") }</Button>
                </Nav>

            </Content>

        </Root>
    );

};