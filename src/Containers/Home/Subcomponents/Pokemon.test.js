/**
 * @desc Dependencias
 */
import React from "react";
import { shallow } from "enzyme";
import 'jest-styled-components'

/**
 * @desc Contenedor
 */
import PokemonComponent from "./Pokemon";

/**
 * @desc Suite de test's
 */
describe("[Pokemon]", () => {
    
	/**
	 * @desc Testeamos el snapshot.
	 */
    it("[Component.Pokemon] Verificamos el renderizado del componente Pokemon", () => {

        // Renderizamos el contenedor.
        const rendered = shallow( <PokemonComponent /> );

        // Comparamos el snapshot
        expect( rendered ).toMatchSnapshot();

    });


});
