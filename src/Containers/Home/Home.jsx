/**
 * @desc Dependencias
 */
import React from "react";

/**
 * @desc Controlador
 */
import HomeController from "./HomeController";

/**
 * @desc Estilos
 */
import {
     Root, 
     Header, Logo,
     Main, Output,
     Languages, Language, LanguageLabel, ImageFlag,
     Nav, Prev, Next, Label
    } from "./Home.styles";

/**
 * @desc Componentes
 */
import Pokemon from "./Subcomponents/Pokemon";
import PokemonDetail from "./Subcomponents/PokemonDetail";

/**
 * @desc Utilidades
 */
import { LanguagesAvailables } from "../../Utils/Constants";
import Translate from "../../Languages";

/**
 * @desc Asset's
 */
import LogoImage from "../../Assets/img/logo.png";
import ESImage from "../../Assets/img/ES.png";
import UKImage from "../../Assets/img/UK.png";

/**
 * @desc Contenedor
 */
class Home extends HomeController {

    /**
     * @desc Renderizamos la pantalla
     */
    render(){

        // Alias del estado
        const { 
            
            // Listado de pokemon's
            pokedex = null,
            // Indicam si se esta cargando.
            loading = false,
            // Lenguajes disponibles
            languageSelected = null
        } = this.state;

        return (
            <Root>

                { /* Cabecera */ }
                <Header>

                    { /* Logotipo */ }
                    <Logo src={ LogoImage } />

                </Header>

                { /* Idioma */ }
                <Languages>
                    
                    
                    { /* Botones de selección de idioma. */ }
                    <Language active={ languageSelected === LanguagesAvailables.ES } onClick={ () => this.changeLanguage( LanguagesAvailables.ES ) }>
                        <ImageFlag src={ ESImage } />
                    </Language>

                    { /* Etiqueta */ }
                    <LanguageLabel>|</LanguageLabel>

                    <Language active={ languageSelected === LanguagesAvailables.EN } onClick={ () => this.changeLanguage( LanguagesAvailables.EN ) }>
                        <ImageFlag src={ UKImage } />
                    </Language>

                </Languages>

                { /* Galeria de pokemon's */ }
                <Main>

                    { /* Recorremos los pokemon's */ }
                    {
                        Array.isArray( pokedex.data ) && pokedex.data.map( ( pokemon, key ) => (
                            <Pokemon data={ pokemon } key={ key } onSelect={ pokemonSelected => this.openDetailPokemon( pokemonSelected ) } />
                        ) )
                    }

                    {
                        loading && 
                            <Output>{ Translate.parse("LOADING") }...</Output>
                    }

                    {
                        !loading &&
                            <Nav>
                                <Prev onClick={ () => this.prevPagination() } disabled={ pokedex.current === 0 }> {"<"} </Prev>
                                <Label>{ Translate.parse("PAGE") } { pokedex.current + 1 }</Label>
                                <Next onClick={ () => this.nextPagination() }> {">"} </Next>
                            </Nav>
                    }

                </Main>

                {
                    pokedex.selected &&
                        <PokemonDetail data={ pokedex.selected } 
                                       language={ languageSelected.code }
                                       onClose={ () => this.closePokemonDetail() } 
                    />
                }

            </Root>
        );

    }

}

export default Home;