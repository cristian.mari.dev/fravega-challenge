/**
 * @desc Dependencias
 */
import React from "react";
import { shallow, mount } from "enzyme";
import 'jest-styled-components'

/**
 * @desc Contenedor
 */
import HomeContainer from "./Home";

/**
 * @desc Utilidades
 */
import { LanguagesAvailables } from "../../Utils/Constants";

/**
 * @desc Suite de test's
 */
describe("[Home]", () => {
    

	/**
	 * @desc La pantalla de inicio.
	 */
    it("[Container.Home] Verificamos el renderizado del container Home", () => {

        // Renderizamos el contenedor.
        const rendered = shallow( <HomeContainer /> );

        // Comparamos el snapshot
        expect( rendered ).toMatchSnapshot();

    });

	/**
	 * @desc Verificamos si la busqueda de una ciudad funciona correctamente.
	 */
    it('[Container.Home] Verificamos el metodo .getAllPokemons() - Con datos reales', async () => {

        // Renderizamos
        const rendered = shallow( <HomeContainer /> ),
              instance= rendered.instance();

        // Llamamos al cambio de ciudad.
        const result = await instance.getAllPokemons( );

        // Verificamos el resultado
        expect( result ).toBeTruthy();

    });
    
	/**
	 * @desc Verificamos si la generación de esquema funciona correctamente.
	 */
    it('[Container.Home] Verificamos el metodo .generateScheme() - Con datos reales', async () => {

        // Pokemon de prueba
        const data = [{
            name: "charmeleon"
        }];

        // Renderizamos
        const rendered = shallow( <HomeContainer /> ),
              instance= rendered.instance();

        // Llamamos al cambio de ciudad.
        const result = await instance.generateScheme( data );

        // Verificamos el resultado
        expect( result ).toEqual( expect.any( Array ) );

    });
    
	/**
	 * @desc Verificamos si la generación de esquema funciona correctamente.
	 */
    it('[Container.Home] Verificamos el metodo .generateScheme() - Con datos erroneos.', async () => {

        // Renderizamos
        const rendered = shallow( <HomeContainer /> ),
              instance= rendered.instance();

        // Errores
        const errors = [null, 1, function(){}, {}, "Picachu"];

        // Recolectamos los errores.
        errors.forEach( async data => {
            expect( await instance.generateScheme( data ) ) .toEqual( false );
        });

    });

	/**
	 * @desc Verificamos si la generación de esquema funciona correctamente.
	 */
    it('[Container.Home] Verificamos el metodo .openDetailPokemon() - Con datos reales', async () => {


        // Renderizamos
        const rendered = mount( <HomeContainer /> ),
              instance= rendered.instance();

        // Pokemon de prueba
        const data = {
            "abilities": [{ "name": "overgrow" }],
            "stats": [{ "name": "hp" }]
        };

        // Llamamos al cambio de ciudad.
        const result = await instance.openDetailPokemon( data );

        // Verificamos el resultado
        expect( result ).toBeTruthy();

    });
    

	/**
	 * @desc Verificamos si la generación de esquema funciona correctamente.
	 */
    it('[Container.Home] Verificamos el metodo .openDetailPokemon() - Con datos erroneos.', async () => {

        // Renderizamos
        const rendered = shallow( <HomeContainer /> ),
              instance= rendered.instance();

        // Errores
        const errors = [null, 1, function(){}, [], "Picachu"];

        // Recolectamos los errores.
        errors.forEach( async data => {
            expect( await instance.openDetailPokemon( data ) ) .toEqual( false );
        });

    });
    
	/**
	 * @desc Verificamos si la busqueda de una ciudad funciona correctamente.
	 */
    it('[Container.Home] Verificamos el metodo .closePokemonDetail()', async () => {

        // Renderizamos
        const rendered = shallow( <HomeContainer /> ),
              instance= rendered.instance();

        // Llamamos al cambio de ciudad.
        const result = await instance.closePokemonDetail( );

        // Verificamos el resultado
        expect( result ).toBeTruthy();

    });
    
	/**
	 * @desc Verificamos si la generación de esquema funciona correctamente.
	 */
    it('[Container.Home] Verificamos el metodo .getDetailsOfPokemon() - Con datos reales', async () => {


        // Renderizamos
        const rendered = mount( <HomeContainer /> ),
              instance= rendered.instance();

        // Pokemon de prueba
        const data = {
            "abilities": [{ "name": "overgrow" }],
            "stats": [{ "name": "hp" }]
        };

        // Llamamos al cambio de ciudad.
        const result = await instance.getDetailsOfPokemon( data );

        // Verificamos el resultado
        expect( result ).toBeTruthy();

    });
    

	/**
	 * @desc Verificamos si la generación de esquema funciona correctamente.
	 */
    it('[Container.Home] Verificamos el metodo .getDetailsOfPokemon() - Con datos erroneos.', async () => {

        // Renderizamos
        const rendered = shallow( <HomeContainer /> ),
              instance= rendered.instance();

        // Errores
        const errors = [null, 1, function(){}, [], "Picachu"];

        // Recolectamos los errores.
        errors.forEach( async data => {
            expect( await instance.getDetailsOfPokemon( data ) ) .toEqual( false );
        });

    });
    
	/**
	 * @desc Verificamos si la generación de esquema funciona correctamente.
	 */
    it('[Container.Home] Verificamos el metodo .getAbilitiesOfPokemon() - Con datos reales', async () => {


        // Renderizamos
        const rendered = shallow( <HomeContainer /> ),
              instance= rendered.instance();

        // Pokemon de prueba
        const data = {
            "abilities": [{ "name": "overgrow" }],
        };

        // Llamamos al cambio de ciudad.
        const result = await instance.getAbilitiesOfPokemon( data );

        // Verificamos el resultado
        expect( result ).toBeTruthy();

    });
    

	/**
	 * @desc Verificamos si la generación de esquema funciona correctamente.
	 */
    it('[Container.Home] Verificamos el metodo .getAbilitiesOfPokemon() - Con datos erroneos.', async () => {

        // Renderizamos
        const rendered = shallow( <HomeContainer /> ),
              instance= rendered.instance();

        // Errores
        const errors = [null, 1, function(){}, [], "Picachu"];

        // Recolectamos los errores.
        errors.forEach( async data => {
            expect( await instance.getAbilitiesOfPokemon( data ) ) .toEqual( false );
        });

    });
    
	/**
	 * @desc Verificamos si la generación de esquema funciona correctamente.
	 */
    it('[Container.Home] Verificamos el metodo .getStatOfPokemon() - Con datos reales', async () => {


        // Renderizamos
        const rendered = shallow( <HomeContainer /> ),
              instance= rendered.instance();

        // Pokemon de prueba
        const data = {
            "stats": [{ "name": "hp" }]
        };

        // Llamamos al cambio de ciudad.
        const result = await instance.getStatOfPokemon( data );

        // Verificamos el resultado
        expect( result ).toBeTruthy();

    });
    

	/**
	 * @desc Verificamos si la generación de esquema funciona correctamente.
	 */
    it('[Container.Home] Verificamos el metodo .getStatOfPokemon() - Con datos erroneos.', async () => {

        // Renderizamos
        const rendered = shallow( <HomeContainer /> ),
              instance= rendered.instance();

        // Errores
        const errors = [null, 1, function(){}, [], "Picachu"];

        // Recolectamos los errores.
        errors.forEach( async data => {
            expect( await instance.getStatOfPokemon( data ) ) .toEqual( false );
        });

    });

	/**
	 * @desc Verificamos si la generación de esquema funciona correctamente.
	 */
    it('[Container.Home] Verificamos el metodo .extractLanguage() - Con datos reales', async () => {

        // Renderizamos
        const rendered = shallow( <HomeContainer /> ),
              instance= rendered.instance();

        // Pokemon de prueba
        const data = {
            "names": [
                {
                  "name": "español",
                  "language": {
                    "name": "es",
                    "url": "https://pokeapi.co/api/v2/language/1/"
                  }
                }
              ]
        };

        // Llamamos al cambio de ciudad.
        const result = instance.extractLanguage( LanguagesAvailables.ES.code, data.names );

        // Verificamos el resultado
        expect( result ).toBeTruthy();

    });
    

	/**
	 * @desc Verificamos si la generación de esquema funciona correctamente.
	 */
    it('[Container.Home] Verificamos el metodo .prevPagination()', async () => {

        // Renderizamos
        const rendered = shallow( <HomeContainer /> ),
              instance= rendered.instance();

        // Valor anterior
        let oldValue = instance.state.pokedex.current;

        // Llamamos al cambio de ciudad.
        const result = await instance.prevPagination( );

        // Verificamos el resultado
        expect( result ).toBeTruthy();
        expect( instance.state.pokedex.current ).toEqual( expect.any( Number ) )
        expect( instance.state.pokedex.current ).toBeGreaterThanOrEqual( 0 );
        expect( instance.state.pokedex.data ).toEqual( expect.any( Array ) )
        expect( oldValue === 0 ? true : oldValue > this.state.pokedex.current ).toBeTruthy()

    });
    

	/**
	 * @desc Verificamos si la generación de esquema funciona correctamente.
	 */
    it('[Container.Home] Verificamos el metodo .nextPagination()', async () => {

        // Renderizamos
        const rendered = shallow( <HomeContainer /> ),
              instance= rendered.instance();

        // Valor anterior
        let oldValue = instance.state.pokedex.current;

        // Llamamos al cambio de ciudad.
        const result = await instance.nextPagination( );

        // Verificamos el resultado
        expect( result ).toBeTruthy();
        expect( instance.state.pokedex.current ).toEqual( expect.any( Number ) )
        expect( instance.state.pokedex.current ).toBeGreaterThanOrEqual( 0 );
        expect( instance.state.pokedex.data ).toEqual( expect.any( Array ) )
        expect( instance.state.pokedex.current > oldValue ).toBeTruthy()

    });
    
});
