/**
 * @desc Dependencias.
 */
import styled, { css } from "styled-components";

/**
 * @desc Raiz.
 */
export const Root = styled.main`
    height:100%;
    margin:0;
`;

/**
 * @desc Contenedor principal.
 */
export const Main = styled.section`
    display:flex;
    justify-content:space-around;
    flex-wrap:wrap;
    flex-direction:column;
    padding-bottom:20px;
`;


/**
 * @desc Mensaje de salida.
 */
export const Output = styled.output`
    display:flex;
    width:100%;
    justify-content:center;
    font-size:1.75rem;
`;

/**
 * @desc Cabecera.
 */
export const Header = styled.header`
    background:#f80000;
    padding:10px;
    text-align:center;
`;

/**
 * @desc Imagenes.
 */
export const Logo = styled.img`
    max-width:128px;
`;

/**
 * @desc Imagenes.
 */
export const Nav = styled.nav`
    display:flex;
    width:100%;
    justify-content:center;
    align-items:center;
`;

/**
 * @desc Paginación previa.
 */
export const Prev = styled.button`
    width:32px;
    height:32px;
    border:2px solid #333;
    border-radius:50%;
    margin:5px;
    cursor:pointer;
    &:hover{
        background:#333;
        color:white;
    }
    ${
        props => props.disabled && 
            css `
                border-color:#666;
                &:hover{
                    background:#666;
                    color:white;
                }
            `
    }
`;

/**
 * @desc Paginación siguiente.
 */
export const Next = styled.button`
    width:32px;
    height:32px;
    border:2px solid #333;
    border-radius:50%;
    margin:5px;
    cursor:pointer;
    &:hover{
        background:#333;
        color:white;
    }
`;


/**
 * @desc Etiqueta.
 */
export const Label = styled.p`
    font-size:1.5rem;
    margin:5px;
`;

/**
 * @desc Lenguajes.
 */
export const Languages = styled.div`
    display:flex;
    background:#1f1f1f;
    color:white;
    justify-content:center;
    padding:0px 10px;
    align-items:center;
`;

/**
 * @desc Lenguaje.
 */
export const LanguageLabel = styled.h1`
    color:#aaa;
    font-size:1.25rem;
    margin:12px;
`;


/**
 * @desc Lenguaje.
 */
export const Language = styled.div`
    position:relative;
    cursor:pointer;
    &:hover::before{
        background:#666;
        content:"";
        width:8px;
        height:8px;
        border-radius:50%;
        position:absolute;
        bottom:-4px;
        left:calc(50% - 4px);
        border:2px solid black;
        border-radius:50%;
    }
    ${
        props => props.active && 
            css `
                &::before{
                    background:white;
                    content:"";
                    width:8px;
                    height:8px;
                    border-radius:50%;
                    position:absolute;
                    bottom:-4px;
                    left:calc(50% - 4px);
                    border:2px solid black;
                    border-radius:50%;
                }
                &:hover::before{
                    background:white;
                }
                &:active::before{
                    background:red;
                }
            `
    }
`;


/**
 * @desc Imagen de banderas
 */
export const ImageFlag = styled.img`
    height:22px;
    margin:0px 5px;
`;
