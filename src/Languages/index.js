/**
 * @desc Utilidades
 */
import { LanguagesAvailables } from "../Utils/Constants";

/**
 * @desc Clase encargada del lenguaje.
 */
class Translate {

    /**
     * @desc Diccionario
     */
    Dictionary = null;

    /**
     * @desc Constructor
     * 
     * @return { Translate }
     */
    constructor(){

        // Lenguaje por defecto.
        this.byDefault = LanguagesAvailables.ES;

        // Cargamos el lenguaje.
        this.loadLanguage( );

        return this;

    }

    /**
     * @desc Traduccimos la palabra
     * 
     * @param { String } key
     * 
     * @return { String }
     */
    parse( key ){

        try{

            // Verificamos que exista un diccionario
            if( this.Dictionary === null )
                throw new Error( ".parse() expected a dictionary valid, but not have nothing." );

            // Verificamos el key
            if( typeof key !== "string" )
                throw new TypeError( ".loadLanguage() expected a param of type string call key, but received a " + typeof key )
            
            // Verificamos que exista la key
            if( this.Dictionary.hasOwnProperty( key.toUpperCase() ) ){
                return this.Dictionary[key];
            }

            throw new Error( ".parse() I search for "+ key +" but it doesn't exist in the dictionary");

        }catch( error ){

            // Logueamos el error
            console.log( error );

            return key;

        }
        
    }

    /**
     * @desc Carga un diccionario
     * 
     * @param { String } code
     * 
     * @return { Translate }
     */
    loadLanguage( code = this.byDefault.code ){

        try{

            // Verificamos el code
            if( typeof code !== "string" ){
                throw new TypeError( ".loadLanguage() expected a param of type string call code, but received a " + typeof code )
            }

            // Indicamos el nuevo diccionario
            this.Dictionary = require( "./" + code + ".json" );

            return true;

        }catch( error ){

            // Logueamos el error
            console.log( error );

            return false;  

        }

    }

}

export default new Translate();